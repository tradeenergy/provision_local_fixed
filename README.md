Aquest playbook deixa una màquina virtual Ubuntu 16.04 (darrera LTS) nova en un estat que permet usar-la per a executar: tradeenergy, customerweb, etc.

Tota la informació que he considerat sensible ha estat posada dins un fitxer encriptat.

### Com usar aquest playbook

Previ a executar ansible s'han de posar les claus ssh a la maquina. 
	$ ssh-copy-id root@novamaquina 
I s'ha d'instalar python-minimal
	root@novamaquina$ apt install python-minimal

Per a fer el provisioning d'una nova màquina i deixar-la llesta per a ser utilitzada.

    $ ansible-playbook provision.yml

Si estàs llegint això ja hauries de saber quin és el password.

### Canvi usuari i password de mysql

Les credencials d'accès a mysql estan guardades en un fitxer encriptat dins del fitxer group_vars/trade.yml. Per a editat el fitxer:

    $ ansible-vault edit group_vars/trade.yml

### Afegir fitxer encriptat per nous grups de màquines

El nom del fitxer coincideix amb el grup de màquines definit a l'inventori hosts. Si en un futur s'afegissin més grups caldria posar les variables sensibles dins d'un altre fitxer.

Per exemple, si suposem que el grup es diguès centos-trade:

    $ ansible-vault create group_vars/centos-trade.yml

### Format del fitxer

Les variables definides dins dels fitxers encriptats segueixen el format:

variable: valor_de_la_variable

MOSTRA:

mysql_username: username

mysql_passwd: password


### Ansible maquina local
    sudo apt install python-minimal ansible

    ansible-playbook -K provision.yml --ask-vault-pass

    7075 vell

